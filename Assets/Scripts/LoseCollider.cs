﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseCollider : MonoBehaviour {

    private LevelManager levelManager;

	// Use this for initialization
	void Start () {
        levelManager = FindObjectOfType<LevelManager>();
	}
	

    void OnTriggerEnter2D(Collider2D other)
    {
        levelManager.LoadLevel("Lose Screen");
    }
}
