﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof(Text))]
public class StarDisplay : MonoBehaviour {

    public int starsAtStart;

    private Text starText;
    private int starCount;
    public enum Status {SUCCESS, FAILURE}

	// Use this for initialization
	void Start () {
        starText = GetComponent<Text>();
        starCount = starsAtStart;
        UpdateDisplay();
	}

    public void AddStars(int amount)
    {
        starCount += amount;
        UpdateDisplay();
    }

    public Status UseStars(int amount)
    {
        if (starCount >= amount)
        {
            starCount -= amount;
            UpdateDisplay();
            return Status.SUCCESS;
        }
            return Status.FAILURE;
    }

    private void UpdateDisplay()
    {
        starText.text = starCount.ToString();
    }
}
