﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderSpawner : MonoBehaviour {

    private GameObject parent;
    private StarDisplay starDisplay;

	// Use this for initialization
	void Start () {
        parent = GameObject.Find("Defenders");
        starDisplay = GameObject.FindObjectOfType<StarDisplay>();

        if (!parent)
        {
            parent = new GameObject("Defenders");
        }
	}
	

    void OnMouseDown()
    {
        {
            Vector2 rawPos = CalculateWorldPointOfMouseClick();
            Vector2 roundedPos = SnapToGrid(rawPos);
            GameObject defender = Button.selectedDefender;


            if (defender)
            {
                int defenderCost = defender.GetComponent<Defender>().starCost;
                if (starDisplay.UseStars(defenderCost) == StarDisplay.Status.SUCCESS)
                {
                    SpawnDefender(roundedPos, defender);
                }
                else
                {
                    Debug.Log("Insufficient stars to spawn");
                }
            }
            else
            {
                Debug.Log("No defender selected");
            }
        }
    }

    private void SpawnDefender(Vector2 roundedPos, GameObject defender)
    {
        Quaternion zeroRot = Quaternion.identity;
        GameObject newDef = Instantiate(defender, roundedPos, zeroRot) as GameObject;

        newDef.transform.parent = parent.transform;
    }

    Vector2 CalculateWorldPointOfMouseClick()
    {
        Vector3 mouse = Input.mousePosition;
        Vector2 worldPos = Camera.main.ScreenToWorldPoint(mouse);
        return worldPos;
    }

    Vector2 SnapToGrid (Vector2 rawWorldPos)
    {
        Vector2 roundWorldPos = new Vector2 (Mathf.Round(rawWorldPos.x), Mathf.Round(rawWorldPos.y));
        return roundWorldPos;
    }
}
